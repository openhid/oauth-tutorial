var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var morgan = require('morgan');
var api = require('./api');

mongoose.connect('mongodb://localhost/test');
var app = express();

app.set('views', path.join(__dirname, '.'));
app.set('view engine', 'jade');
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/api', api.api);
app.use(function (error, request, response, next) {
    error.status = 404;
    next(error);
});

app.use(function (error, request, response, next) {
    response.status(error.status || 500);
    response.render('error', {
        message: error.message,
        error: error
    });
});

console.log('Listening on port 3000');
app.listen(3000);
