var mongoose = require('mongoose');
var WeatherSchema = new mongoose.Schema({
    coord: {
        lon: Number,
        lat: Number 
    },
    weather: [{
        id: Number, 
        main: String, 
        description: String, 
        icon: String 
    }],
    base: String, 
    main: {
        temp: Number, 
        pressure: Number,
        humidity: Number,
        temp_min: Number, 
        temp_max: Number
    },
    wind: {
        speed: Number,
        deg: Number, 
        gust: Number
    },
    clouds: {
        all: Number 
    },
    dt: Date,
    sys: {
        type: { type: Number },
        id: Number,
        message: Number,
        country: String,
        sunrise: Number,
        sunset: Number 
    },
    id: Number,
    name: String,
    cod: Number 
});

exports.Weather = mongoose.model('Weather', WeatherSchema);
