var request = require('request');
var express = require('express');
var mongoose = require('mongoose');

var Weather = require('./weather').Weather;

exports.api = express.Router();
exports.api.route('/refresh')
.get(function (apiRequest, apiResponse, next) {
    request('http://api.openweathermap.org/data/2.5/weather?q=Miami,uk&appid=d0f149f0c8f0afac7794544fd00a92f3', function (error, response, body) {
        if (error)
            return next(error);

        if (response.statusCode != 200) {
            weatherError = new Error('Cannot update weather');
            weatherError.status = response.statusCode;

            return next(weatherError);
        }

        var weather = new Weather(JSON.parse(body));
        weather.save(function (error, weather) {
            if (error)
                return next(error);

            apiResponse.json(weather);
        });
    });
});

exports.api.route('/all')
.get(function (apiRequest, apiResponse, next) {
    Weather.find(function (error, weathers) {
        if (error)
            return next(error);
        apiResponse.json(weathers);
    });
});

exports.api.route('/last')
.get(function (apiReqest, apiResponse, next) {
    Weather.findOne({}).sort({ dt: -1 }).exec(function (error, weather) {
        if (error)
            return next(error);

        apiResponse.json(weather);
    });
});
