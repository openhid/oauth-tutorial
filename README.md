# oath-tutorial

```
git clone https://gitlab.com/openhid/oauth-tutorial
cd oauth-tutorial
npm install
mongod --dbpath data/db
```
Update `app.js` line 10 with your app id from https://home.openweathermap.org/
```
node app.js
```

- Launches on port 3000
- `GET /api/all` responds with all weather data stored in db
- `GET /api/refresh` fetches new weather, stores and returns it
- `GET /api/last` responds with last stored weather data
